resource "aws_iam_role" "PublishFlowLogs" {
  name = "PublishFlowLogs"
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal":
          {
            "Service": "vpc-flow-logs.amazonaws.com"
          },
        "Action": "sts:AssumeRole"
       }
    ]
})
}


resource "aws_iam_policy" "Permissions-Policy-For-VPCFlowLogs" {
  name        = "Permissions-Policy-For-VPCFlowLogs"
  description = "Permissions-Policy-For-VPCFlowLogs"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
            "logs:DescribeLogGroups",
            "logs:DescribeLogStreams"
            ],
        "Effect": "Allow",
        "Resource": "*"
        }
   ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "Permissions-Policy-For-VPCFlowLogs-attach" {
  role       = aws_iam_role.PublishFlowLogs.name
  policy_arn = aws_iam_policy.Permissions-Policy-For-VPCFlowLogs.arn
}

resource "aws_cloudwatch_log_group" "vpc-flow-logs" {
  name = "vpc-flow-logs"
}

resource "aws_flow_log" "example" {
  log_destination      = aws_cloudwatch_log_group.vpc-flow-logs.arn
  log_destination_type = "cloud-watch-logs"
  traffic_type         = "ALL"
  vpc_id               = var.vpcid
  log_format           = "$${version} $${account-id} $${vpc-id} $${subnet-id} $${instance-id} $${interface-id} $${srcaddr} $${dstaddr} $${srcport} $${dstport} $${pkt-srcaddr} $${pkt-dstaddr} $${protocol} $${type} $${packets} $${bytes} $${start} $${end} $${action} $${tcp-flags} $${log-status} $${region} $${az-id} $${sublocation-type} $${sublocation-id}"
}

resource "aws_cloudwatch_log_subscription_filter" "test_lambdafunction_logfilter" {
  name            = "AllTraffic"
  role_arn        = aws_iam_role.PublishFlowLogs.arn
  log_group_name  = "vpc-flow-logs"
  filter_pattern  = ""
  destination_arn = "arn:aws:logs:us-east-1:111111111111:destination:myDestination"
  distribution    = "Random"
}
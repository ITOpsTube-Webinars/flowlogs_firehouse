variable "logging_bucket" {
  description = "Application Logging bucket name"
  default     = "applogfirehosebucket"
}


resource "aws_s3_bucket" "applogfirehosebucket" {
  bucket = "${var.logging_bucket}"
  acl    = "private"
}

resource "aws_iam_role" "FirehosetoS3Role" {
  name = "FirehosetoS3Role"
  assume_role_policy = jsonencode({
    "Statement": {
        "Effect": "Allow",
        "Principal": {
            "Service": "firehose.amazonaws.com"
        },
        "Action": "sts:AssumeRole",
        "Condition": {
            "StringEquals": {
                "sts:ExternalId": "111111111111"
            }
        }
    }
  })
}


resource "aws_iam_policy" "Permissions-Policy-For-Firehose" {
  name        = "Permissions-Policy-For-Firehose"
  description = "Permissions-Policy-For-Firehose"

  policy = <<EOF
{
  "Statement": [
    {
      "Effect":
    "Allow",
      "Action": [
          "s3:AbortMultipartUpload",
          "s3:GetBucketLocation",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:ListBucketMultipartUploads",
          "s3:PutObjectAcl",
          "s3:PutObject"
    ],
      "Resource": [
          "arn:aws:s3:::${applogfirehosebucket}",
          "arn:aws:s3:::${applogfirehosebucket}/*" ]
    }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "Permissions-Policy-For-Firehose-attach" {
  role       = aws_iam_role.FirehosetoS3Role.name
  policy_arn = aws_iam_policy.Permissions-Policy-For-Firehose.arn
}


resource "aws_kinesis_firehose_delivery_stream" "my-delivery-stream" {
  name        = "my-delivery-stream"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.FirehosetoS3Role.arn
    bucket_arn = aws_s3_bucket.applogfirehosebucket.arn
  }
}

resource "aws_iam_role" "CWLtoKinesisFirehoseRole" {
  name = "CWLtoKinesisFirehoseRole"
  assume_role_policy = jsonencode({
  "Statement": {
    "Effect": "Allow",
    "Principal": {
  "Service": [
    "logs.us-east-1.amazonaws.com",
    "logs.us-east-2.amazonaws.com"
    ]
  },
    "Action": "sts:AssumeRole"
  }
  })
}

resource "aws_iam_policy" "Permissions-Policy-For-CWL" {
  name        = "Permissions-Policy-For-CWL"
  description = "Permissions-Policy-For-CWL"

  policy = <<EOF
{
    "Statement":[
      {
        "Effect":"Allow",
        "Action":["firehose:*"],

    "Resource":["arn:aws:firehose:us-east-1:111111111111:*"]
      },
      {
        "Effect":"Allow",
        "Action":["iam:PassRole"],

    "Resource":["arn:aws:iam::111111111111:role/CWLtoKinesisFirehoseRole"]
      }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "CWLtoKinesisFirehoseRole-attach" {
  role       = aws_iam_role.CWLtoKinesisFirehoseRole.name
  policy_arn = aws_iam_policy.Permissions-Policy-For-CWL.arn
}

resource "aws_cloudwatch_log_destination" "test_destination" {
  name       = "test_destination"
  role_arn   = aws_iam_role.CWLtoKinesisFirehoseRole.arn
  target_arn = aws_kinesis_firehose_delivery_stream.my-delivery-stream.arn
}

data "aws_iam_policy_document" "test_destination_policy" {
  statement {
    effect = "Allow"
    principals {
      type = "AWS"
      identifiers = [
        "222222222222",
      ]
    }
    actions = [
      "logs:PutSubscriptionFilter",
    ]
    resources = [
      aws_cloudwatch_log_destination.test_destination.arn,
    ]
  }
}

resource "aws_cloudwatch_log_destination_policy" "test_destination_policy" {
  destination_name = aws_cloudwatch_log_destination.test_destination.name
  access_policy    = data.aws_iam_policy_document.test_destination_policy.json
}

